package Project;

class EnergyLoss
{
    
    double K ;
    double m_e ;
    double I ; 
    double Z;
    double A;
    double rho;
    double b; 
    double gamma; 
    double m;
    
    public EnergyLoss(double rho_in, double Z_in, double A_in)  
   {
       Z = Z_in;
       A = A_in;
       rho = rho_in;
   }  



    public double getEnergyLoss(Particle p)  
    {
        
        if( A == 0 || Z == 0 || rho == 0){
        return 0.;
        }
        
        K = 0.307075;
        m_e = 0.511;
        I = 0.0000135 * Z;  
        b = p.beta();
        gamma = p.gamma();
        m = p.m ;
         
        return F()*S()*100; 
        
    }
        
    
    double F() 
    { 
       
        return (K*rho*(Z/A)*(1/(b*b)))  ; 
       
        
    }
        
    double W()
    {
        return ((2*m_e*b*b*gamma*gamma)/(1+(2*gamma*(m_e/m))+(m_e/m)*(m_e/m)));  
        
    }
    
    
    double L()
    {
        return (Math.log(2*m_e*b*b*gamma*gamma*W()/(I*I)));
    }
     
    
    
    double S()
    { 
        
      return (0.5*L())-(b*b) ;    
        
    }
    
    
   
    
} 
