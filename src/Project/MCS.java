package Project;


class MCS
{
    double K ;
    double m_e ;
    double I ; 
    double Z;
    double A;
    double rho;
    double b; 
    double gamma; 
    double m; 
    double p;
    double z;
    
    public MCS(double rho_in, double Z_in, double A_in)
    {
        Z = Z_in;
       A = A_in;
       rho = rho_in; 
    }

    public double getX0()
    {
        
       
           return X0()/100;
    }

    
    
        double X0()
        {
            if (rho == 0 || A == 0 || Z == 0){
            return 0.;
            }
            
            return ((716.4*A)/(rho*Z*(Z+1)*Math.log(287/Math.sqrt(Z))));
            
           
              
            
            
        }

      

    public double getTheta0(Particle part, double x)
    {
       
          b = part.beta() ;
          p = part.momentum();
          z = part.Q; 
          
          if (rho == 0 || A == 0 || Z == 0){
          return 0.;
          }
          
        
        return (Theta0F(x)*Theta0S(x)); 
    }
    
    
    
       double Theta0F(double x)
       {
           double X0 = getX0();
           return (((13.6)/(b*p))*(z*Math.sqrt(x/X0))) ;
       }
       
       double Theta0S(double x)
       {
           double X0 = getX0();
           return (1+(0.038*Math.log(x/X0))); 
       }
}  

