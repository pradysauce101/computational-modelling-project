package Project;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Random;

class RunSimulation
{
    // Program to run a simulation of particles in a "experiment"

    // The program makes use of the class Particle (almost identical to the one used in week 4)
    // to store the components of the position fourvector (time, x, y, z)
    // and the four-momentum (technically stored as mass and px, py, pz)

    // The particle tracking is performed by the class ParticleTracker,
    // which is almost identical to the class used in week 4, however it is extended
    // to incorporate energy loss, multiple scattering
    // and a simple adaptive algorithm to ensure that single steps end approximately
    // at boundaries betwen different experimental features
    
    // The "experimental geometry" is defined in the class Geometry:
    //    * An experiment is a collection of "volumes", that are numbered with a unique
    //      identifier from 0 to the number of experimental features.
    //    * Currently all experiemntal features are cuboids of different sizes and materials,
    //      with the sides aligned to the coordinate system axes. The example is a block of iron
    //      (user-definable length) + two "planar detectors"
    //    * Internally to the "Geometry" class are two helper classes that implement the
    //      formulas for calculation of energy loss (class "Energy loss")
    //      and multiple scattering angles (class "MCS")
    //    * The main functionality is to check, if a certain particle is inside a certain volume
    //      and apply energy loss and multiple scattering during simulation
    //    * The class also provides a simple mechanism to detect changes in the volume as
    //      the particle propagates and suggest an adapted step length to keep one step within
    //      one volume (the granularity of this scan is adjusted with "minfeaturesize")
    //    * At the end, the class is used to "detect" particle signals in certain volumes (detectors)
    //
    //
    // At the end of the simulation of each event, one may analyse the
    // results and fill histograms. Examples are provided to perform calculations using the:
    //    * Generated particles (Particles_gen)
    //    * Simulated particles (Particles_sim) - these include the effect of energy loss and
    //      multiple scattering
    //    * Detector response (Particles_det) - these provide measurement points that can be
    //      further used to reconstruct particles like in a real experiment, where
    //      Particles_gen and Particles_sim are unknown

    // parameters used for the ParticleTracker
    // total time to track (seconds), number of time steps, use/don't use RK4
    static final double time = 1E-8;
    static final int nsteps = 1000;
    static final boolean useRungeKutta4 = false; 

    // minimum size of experimental features,
    // ensure this is a factor ~10 smaller than the thinest elements of the experiment
    static final double minfeaturesize = 0.0005;

    // start values
    static final double startMomentum = 1000.; // MeV
    static final double startAngle = 0.0;      // Radians
    
    // Number of events to simulate
    static int numberOfEvents = 104753;


    static String fileNameDefined = "Zprimeto2mu_1.csv";
    static File file;
    static Scanner inputStream;

    public static void main(String[] args) {
    
 
    try {
        file = new File(fileNameDefined);
        inputStream = new Scanner(file);
    } catch (IOException e) {
        System.err.println("Problem reading file " + fileNameDefined);
    }
    
       Random randGen = new Random();
    
    
   
        // setup histograms for analysis
        Histogram hist_gen_mom = new Histogram(50, 0., startMomentum*1.01, "initial generated Momentum");
        Histogram hist_sim_mom = new Histogram(50, 0., startMomentum*1.01, "simulated final Momentum");
        Histogram hist_E = new Histogram(500, 2000., 4000., "Generated Mass");
        Histogram hist_E1 = new Histogram(500, 2000., 4000., "Generated Mass1");
        Histogram hist_E2 = new Histogram(500, 2000., 4000., "Generated Mass2");

        // Define the genotrical properties of the experiment in method SetupExperiment()
        Geometry Experiment = SetupExperiment();
        
        // start of main loop: run the simulation numberOfEvents times
        for (int nev = 0; nev < numberOfEvents; nev++) {

            if (nev % 1000 == 0) {
                System.out.println("Simulating event " + nev);
            }

            // get the particles of the event to simulate
            Particle [] Particles_gen = GetParticles();
            
            // simulate propagation of each generated particle,
            // store output in Particles_sim and Tracks_sim
            Particle [] Particles_sim = new Particle[Particles_gen.length];
          
            Track [] Tracks_sim = new Track[Particles_gen.length];
          
            for (int ip = 0; ip < Particles_gen.length; ip++) {
                // some output (need to disable before running large numbers of events!)
                // System.out.println("Simulating particle " + ip + " of event " + nev);
                // Particles_gen[ip].print();

                ParticleTracker tracker = new ParticleTracker(Particles_gen[ip], time, nsteps, useRungeKutta4);

                Particles_sim[ip] = tracker.track(Experiment);
                
                
                // System.out.println("Output particle");
                // Particles_sim[ip].print();

                // save the full simulated track for later analysis
                Tracks_sim[ip] = tracker.getTrack();
                

                // write scatter plot for event 0, particle 0 to disk into file "output_particle.csv"
                if (nev == 0 && ip == 0) {
                    Tracks_sim[ip].writeToDisk("output_particle_week5.csv");
                }
                
                if (nev == 0) {
                    Tracks_sim[ip].writeToDisk("Particle_Track1"+ip+".csv");
                }
            }
            
            
            
            
            // end of simulated particle propagation

            // simulate detection of each particle in each element from the simulated tracks
            // this is just for dumping the simulation to the screen
            // for (int ip = 0; ip < Tracks_sim.length; ip++) {
            //  double [][] detection_txyz = Experiment.detectParticles(Tracks_sim[ip]);

            //     for (int idet = 1; idet < Experiment.getNshapes(); idet++) {
            //         System.out.println("Particle " + ip + " detection in volume " + idet);
            //         System.out.println("(t,x,y,z) = (" + detection_txyz[idet][0] + ", "
            //                         + detection_txyz[idet][1] + ", "
            //                         + detection_txyz[idet][2] + ", "
            //                         + detection_txyz[idet][3] + ")");
            //     }
            // }

            // at this stage the simulation is done and we analyse the output
            // typically we don't want to store thousands of tracks,
            // but rather calculate some interesting quantities and make histograms of the distributions

            // the following analysis is specific to single-particle events with two "detectors"
            // it would look different in more complex cases

            // retrieve initial generated particle momentum and fill histogram
            hist_gen_mom.fill(Particles_gen[0].momentum());
            
            
            // retrieve simulated particle momentum at the end of the simulation and fill histogram
            
            hist_sim_mom.fill(Particles_sim[0].momentum());
             
            
            // calculate theta angles in the z-x and z-y planes
            // theta ~ atan2(x, z)
 

            // after detection: reconstruct the angle from the two detected positions!
            // the detectors have volume number 2+3 (see printout)
            double [][] detection_txyz = Experiment.detectParticles(Tracks_sim[0]);
            double x_det2 = detection_txyz[2][1]; // x-coo in detector 2
            double x_det3 = detection_txyz[3][1]; // x-coo in detector 3
            double y_det2 = detection_txyz[2][2]; // y-coo in detector 2
            double y_det3 = detection_txyz[3][2]; // y-coo in detector 3
            double z_det2 = detection_txyz[2][3]; // z-coo in detector 2
            double z_det3 = detection_txyz[3][3]; // z-coo in detector 3
            double det_theta_zx = 0.;

            // end of analysis
            
            double sumE = Particles_gen[0].E() + Particles_gen[1].E();
            double sumpx = Particles_gen[0].px + Particles_gen[1].px;
            double sumpy = Particles_gen[0].py + Particles_gen[1].py;
            double sumpz = Particles_gen[0].pz + Particles_gen[1].pz;
            
            double sumE1 = Particles_sim[0].E() + Particles_sim[1].E();
            double sumpx1 = Particles_sim[0].px + Particles_sim[1].px;
            double sumpy1 = Particles_sim[0].py + Particles_sim[1].py;
            double sumpz1 = Particles_sim[0].pz + Particles_sim[1].pz; 
            
            
                    
            hist_E.fill(Math.sqrt(sumE*sumE-sumpx*sumpx-sumpy*sumpy-sumpz*sumpz)/1000.); // convert to GeV, so numbers have less zeros
            hist_E1.fill(Math.sqrt(sumE1*sumE1-sumpx1*sumpx1-sumpy1*sumpy1-sumpz1*sumpz1)/1000.);
            
            
             Particle [] Particles_smear = new Particle[2];
             for(int ip=0;  ip < 2; ip++) {
               
                     Particles_smear [ip] = new Particle();
                     
                    
                     double reso = Math.sqrt((0.025*0.025)+(0.0001*0.0001)*(Particles_smear[ip].px*Particles_smear[ip].px+Particles_smear[ip].py*Particles_smear[ip].py));  // this is a constant 2% resolution, can make this better - see below!
                     double smear = randGen.nextGaussian()*reso+1.;
                     Particles_smear[ip].px =  Particles_gen[ip].px*smear;
                     Particles_smear[ip].py =  Particles_gen[ip].py*smear;
                     Particles_smear[ip].pz =  Particles_gen[ip].pz*smear;
                     
               // System.out.println("Resolution =" + reso);
               // System.out.println("Resolution =" + Particles_smear[ip].px*Particles_smear[ip].px+Particles_smear[ip].py*Particles_smear[ip].py);
                  
            }
           
                    double sumE2 = Particles_smear[0].E() + Particles_smear[1].E();
                    double sumpx2 = Particles_smear[0].px + Particles_smear[1].px;
                    double sumpy2 = Particles_smear[0].py + Particles_smear[1].py;
                    double sumpz2 = Particles_smear[0].pz + Particles_smear[1].pz;
                    
            hist_E2.fill(Math.sqrt(sumE2*sumE2-sumpx2*sumpx2-sumpy2*sumpy2-sumpz2*sumpz2)/1000.);
            
            

        }
        // end of main event loop

        // write out histograms for plotting and futher analysis
        hist_gen_mom.writeToDisk("gen_mom.csv");
        hist_sim_mom.writeToDisk("sim_mom.csv");
        hist_E.writeToDisk("Generated Mass.csv");
        hist_E1.writeToDisk("Generated Mass1.csv"); 
        hist_E2.writeToDisk("Generated Mass2.csv");
       
    } 
    

    public static Geometry SetupExperiment ()
    {
        // example setup the experiment

        final double ironThickness = 1.0;
        Geometry Experiment = new Geometry(minfeaturesize);
        
        // this line defines the size of the experiment in vacuum
        Experiment.AddCylinder(0, 0, 0.,                      // start r, phi, z
                             1.2, 2*Math.PI, 0.,              // end   r, phi, z
                             0., 0., 0.);                     // zeros for "vacuum"

        // block of iron: 0.4x0.4 m^2 wide in x,y-direction, ironThickness m thick in z-direction
        Experiment.AddCylinder(1.20, 0., 0.,            // start r, phi, z
                             2.2, 2*Math.PI, 0,   // end   r, phi, z
                             7.87, 26, 55.845);           // density, Z, A
        
        // three 1mm-thin "silicon detectors" 10cm, 20cm and 30cm after the iron block
        Experiment.AddCylinder(2.2, 0., 0., // start r, phi, z
                             2.1, 2*Math.PI,0. ,   // end   r, phi, z
                             2.33, 14, 28.085);                 // density, Z, A
        
        Experiment.AddCylinder(2.3, 0., ironThickness+0.200, // start r, phi, z
                             2.31, 2*Math.PI, 0. ,   // end   r, phi, z
                             2.33, 14, 28.085);                 // density, Z, A
        
        Experiment.AddCylinder(2.4, 0. , 0. , // start r, phi, z
                             2.41, 2*Math.PI, 0.,   // end   r, phi, z
                             2.33, 14, 28.085); 
        
        Experiment.Print();

        return Experiment;
    }
    
 
    

 

    public static Particle[] GetParticles()
    {
    // argument specifies the line number of the file to return

 

        Particle [] Particles_gen = new Particle[2];

 

    String data = inputStream.next();
    
    String[] values = data.split(",");
    //System.out.println(values[0]+ " " +values[1]+" "+values[2] +" "+values[3]);
    
    Particles_gen[0] = new Particle();
    Particles_gen[0].px = Double.valueOf(values[0]);
    Particles_gen[0].py = Double.valueOf(values[1]);
    Particles_gen[0].pz = Double.valueOf(values[2]);
    Particles_gen[0].m = 106.;
    Particles_gen[0].Q = Integer.valueOf(values[3]);
    
    Particles_gen[1] = new Particle();
    Particles_gen[1].px = Double.valueOf(values[4]);
    Particles_gen[1].py = Double.valueOf(values[5]);
    Particles_gen[1].pz = Double.valueOf(values[6]);
    Particles_gen[1].m = 106.;
    Particles_gen[1].Q = Integer.valueOf(values[7]);
    
    return Particles_gen;
    }
    
    
    
 }
    


  
  